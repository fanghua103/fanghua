# -*- coding: utf-8 -*-

from django.conf.urls import patterns

urlpatterns = patterns(
    'home_application.views',
    (r'^$', 'home'),
    (r'^history/$', 'show_history'),
    (r'^dev-guide/$', 'dev_guide'),
    (r'^contactus/$', 'contactus'),
    (r'^api/get_hosts$', 'get_hosts'),
    (r'^api/execute/$', 'execute'),
    (r'^api/get_operations/$', 'get_operations'),
    (r'^api/get_log/(?P<operation_id>\d+)/$', 'get_log'),
)
