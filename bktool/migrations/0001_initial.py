# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Operation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('biz', models.CharField(max_length=30)),
                ('user', models.CharField(max_length=50)),
                ('script_name', models.CharField(max_length=50)),
                ('argument', models.CharField(max_length=30, null=True, blank=True)),
                ('status', models.CharField(default=b'queue', max_length=30)),
                ('start_time', models.DateTimeField(auto_now_add=True)),
                ('machine_numbers', models.IntegerField()),
                ('log', models.TextField(null=True, blank=True)),
                ('end_time', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='Script',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('script', models.TextField()),
                ('argument', models.CharField(max_length=30, null=True, blank=True)),
                ('info', models.CharField(max_length=128)),
            ],
        ),
    ]
