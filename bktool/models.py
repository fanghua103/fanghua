from django.db import models

# Create your models here.

class Script(models.Model):
    name = models.CharField(max_length=128)
    script = models.TextField()
    argument = models.CharField(max_length=30, null=True, blank=True)
    info = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name

class Operation(models.Model):
    biz = models.CharField(max_length=30)
    user = models.CharField(max_length=50)
    script_name = models.CharField(max_length=50)
    argument = models.CharField(max_length=30, null=True, blank=True)
    status = models.CharField(max_length=30, default="queue")
    start_time = models.DateTimeField(auto_now_add=True)
    machine_numbers = models.IntegerField()
    log = models.TextField(null=True, blank=True)
    end_time = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return self.user + self.script.name

    class Meta:
        ordering = ['-id']
